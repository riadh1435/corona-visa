import { Component, OnInit } from '@angular/core';
import * as L from 'leaflet';
import { ShapeService } from '../maps/shape.service';

@Component({
    selector: 'icons-cmp',
    moduleId: module.id,
    templateUrl: 'icons.component.html',
    styleUrls: ['./icons.component.scss']
})

export class IconsComponent implements OnInit {

    map: any;
    private states;

    constructor(private shapeService: ShapeService) { }

    ngOnInit() {
        this.map = L.map('map-z', {
            center: [24.64015, 46.70357],
            zoom: 6
        });

        const tiles = L.tileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        });

        tiles.addTo(this.map);

        this.shapeService.getStateShapes().subscribe(states => {
            this.states = states;
            this.initStatesLayer();
        });


    }

    getColor(){
        let color: string[] = ['red', 'green', 'orange', 'grey'];
        const randomNumber = Math.floor(Math.random() * 4);
        return color[randomNumber];
    }

    private initStatesLayer() {
        const stateLayer = L.geoJSON(this.states, {
            style: (feature) => ({
                weight: 3,
                opacity: 0.5,
                color: '#008f68',
                fillOpacity: 0.8,
                fillColor: this.getColor()
            }),
            onEachFeature: (feature, layer) => (
                layer.on({
                    mouseover: (e) => (this.highlightFeature(e)),
                    mouseout: (e) => (this.resetFeature(e)),
                })
            )
        });

        this.map.addLayer(stateLayer);
        stateLayer.bringToBack();
    }

    private highlightFeature(e) {
        const layer = e.target;
        layer.setStyle({
            weight: 10,
            opacity: 1.0,
            color: 'grey',
            fillOpacity: 0.5,
            fillColor: 'grey',
        });
    }

    private resetFeature(e) {
        const layer = e.target;
        layer.setStyle({
            weight: 3,
            opacity: 0.5,
            color: '#008f68',
            fillOpacity: 0.8,
            fillColor: 'green'
        });
    }
}
