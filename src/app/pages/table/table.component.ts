import { Component, OnInit } from '@angular/core';
import * as L from 'leaflet';
import { interval } from 'rxjs';

declare interface TableData {
    headerRow: string[];
    dataRows: string[][];
}

@Component({
    selector: 'table-cmp',
    moduleId: module.id,
    templateUrl: 'table.component.html',
    styleUrls: ['./table.component.scss']
})

export class TableComponent implements OnInit {
    name = 'Angular';
    map: any;

    // Counter for itteration 
    cursor = 0;
    // Counter stop 
    stop;
    // Enable Play 
    enablePlay = false;
    // 
    markers = [];
    polylines = [];

    indexStation = 10;

    ngOnInit() {
        this.map = L.map('map-p', {
            center: [24.64015, 46.70357],
            zoom: 13
        });

        const tiles = L.tileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        });

        tiles.addTo(this.map);

        var dotlayer, dots, gdotlayer, gdots, dotcount = 1000, gdotcount = 75;
        var centerlat = 24.64015, centerlon = 46.70357;
        make_dots(), make_gdots();
        var dotStyleDefault = {
            radius: 5,
            fillColor: 'green',
            color: "#000",
            weight: 0,
            opacity: 1,
            fillOpacity: .9
        }, dotStyleHighlight = {
            radius: 6,
            fillColor: "red",
            color: "#FFF",
            weight: 1,
            opacity: 1,
            fillOpacity: .9
        }, gdotStyleDefault = {
            radius: 5,
            fillColor: "red",
            color: "#FFF",
            weight: 0,
            opacity: 1,
            fillOpacity: .9
        }, gdotStyleHighlight = {
            radius: 6,
            fillColor: "#da3e7b",
            color: "#FFF",
            weight: 1,
            opacity: 1,
            fillOpacity: .9
        };

        dotlayer = L.geoJson(dots, {
            pointToLayer: function (feature, latlng) {
                return L.circleMarker(latlng, dotStyleDefault);
            },
            onEachFeature: onEachDot
        }).addTo(this.map), gdotlayer = L.geoJson(gdots, {
            pointToLayer: function (feature, latlng) {
                return L.circleMarker(latlng, gdotStyleDefault);
            },
            onEachFeature: onEachGdot
        }).addTo(this.map);

        function normish(mean, range) {
            var num_out = (Math.random() + Math.random() + Math.random() + Math.random() - 2) / 2 * range + mean;
            return num_out;
        }

        function make_dots() {
            dots = {
                type: "FeatureCollection",
                features: []
            };
            for (var i = 0; i < dotcount; ++i) {
                const x = normish(0, 5), y = normish(0, 5);
                var g = {
                    type: "Point",
                    coordinates: [.1 * x + centerlon, .1 * y + centerlat]
                }, p = {
                    id: i,
                    popup: "Mohamed_" + i,
                    year: parseInt('' + (100 * Math.sqrt(x * x + y * y) * (1 - Math.random() / 2) + 1950))
                };
                dots.features.push({
                    geometry: g,
                    type: "Feature",
                    properties: p
                });
            }
        }

        function make_gdots() {
            gdots = {
                type: "FeatureCollection",
                features: []
            };
            for (var i = 0; i < gdotcount; ++i) {
                const x = normish(0, 1), y = normish(0, 1);
                var g = {
                    type: "Point",
                    coordinates: [.05 * x + centerlon + .05, .05 * y + centerlat]
                }, p = {
                    id: i,
                    popup: "Khaled_" + i,
                    year: parseInt('' + (100 * Math.sqrt(x * x + y * y) * (1 - Math.random() / 2) + 1900))
                };
                gdots.features.push({
                    geometry: g,
                    type: "Feature",
                    properties: p
                });
            }
        }

        function highlightDot(e) {
            var layer = e.target;
            layer.setStyle(dotStyleHighlight), L.Browser.ie || L.Browser.opera || layer.bringToFront();
        }
        function resetDotHighlight(e) {
            var layer = e.target;
            layer.setStyle(dotStyleDefault);
        }
        function onEachDot(feature, layer) {
            layer.on({
                mouseover: highlightDot,
                mouseout: resetDotHighlight
            }), layer.bindPopup('<table style="width:150px"><tbody><tr><td><div><b>name:</b></div></td><td><div>' + feature.properties.popup + "</div></td></tr><tr class><td><div><b>year:</b></div></td><td><div>" + feature.properties.year + "</div></td></tr></tbody></table>");
        }
        function highlightGdot(e) {
            var layer = e.target;
            layer.setStyle(gdotStyleHighlight), L.Browser.ie || L.Browser.opera || layer.bringToFront();
        }
        function resetGdotHighlight(e) {
            var layer = e.target;
            layer.setStyle(gdotStyleDefault);
        }
        function onEachGdot(feature, layer) {
            layer.on({
                mouseover: highlightGdot,
                mouseout: resetGdotHighlight
            }), layer.bindPopup('<table style="width:150px"><tbody><tr><td><div><b>name:</b></div></td><td><div>' + feature.properties.popup + "</div></td></tr><tr class><td><div><b>year:</b></div></td><td><div>" + feature.properties.year + "</div></td></tr></tbody></table>");
        }

    }

    points = [
        {
            "latitude": 24.792500746930745,
            "longitude": 46.7215919494629,
            "status": "moving"
        },
        {
            "latitude": 24.785176062216454,
            "longitude": 46.70459747314454,
            "status": "moving"
        },
        {
            "latitude": 24.777383369753395,
            "longitude": 46.68657302856445,
            "status": "idle"
        },
        {
            "latitude": 24.793279943257993,
            "longitude": 46.67901992797852,
            "status": "no-communication"
        },
        {
            "latitude": 24.785799456474443,
            "longitude": 46.66065216064454,
            "status": "idle"
        },
        {
            "latitude": 24.7694343194148,
            "longitude": 46.66786193847657,
            "status": "stopped"
        },
        {
            "latitude": 24.75306702526595,
            "longitude": 46.677303314208984,
            "status": "moving"
        },
        {
            "latitude": 24.760237724328665,
            "longitude": 46.69515609741212,
            "status": "in-active"
        },
        {
            "latitude": 24.744648720102663,
            "longitude": 46.70339584350587,
            "status": "idle"
        },
        {
            "latitude": 24.739815731767283,
            "longitude": 46.69378280639649,
            "status": "stopped"
        },
        {
            "latitude": 24.736229844832458,
            "longitude": 46.68537139892578,
            "status": "moving"
        },
        {
            "latitude": 24.733579340205964,
            "longitude": 46.67644500732422,
            "status": "stopped"
        },
        {
            "latitude": 24.74075116352642,
            "longitude": 46.673698425292976,
            "status": "idle"
        },
        {
            "latitude": 24.735606201766874,
            "longitude": 46.66065216064454,
            "status": "moving"
        },
        {
            "latitude": 24.727498557349683,
            "longitude": 46.66374206542969,
            "status": "idle"
        },
        {
            "latitude": 24.72375638939712,
            "longitude": 46.65464401245117,
            "status": "stopped"
        }
    ];



    // Start marker	
    startIcon = L.icon({
        iconUrl: './assets/img/pin.svg',
        iconSize: [60, 60] // size of the icon
    });

    //Draw line 
    drawLine(map, index) {

        if (index > 0) {
            var lastPoint = new L.LatLng(this.points[index - 1].latitude, this.points[index - 1].longitude);
            var currentPoint = new L.LatLng(this.points[index].latitude, this.points[index].longitude);
            var pointList = [lastPoint, currentPoint];

            var firstpolyline = new L.Polyline(pointList, {
                color: '#61337F',
                weight: 3,
                opacity: 0.5,
                smoothFactor: 1
            });
            firstpolyline.addTo(map);

            this.polylines.push(firstpolyline);



        }

    };


    // Draw Marker 
    drawMarker(map, index) {
        var latitude = this.points[index].latitude;
        var longitude = this.points[index].longitude;
        map.setView([latitude, longitude], 15);
        var marker;
        marker = L.marker([latitude, longitude], { icon: this.startIcon }).addTo(map);
        this.markers.push(marker);
        var circle = L.circle([latitude, longitude], {
            color: 'red',
            fillColor: '#f03',
            fillOpacity: 0.5,
            radius: 500
        }).addTo(this.map);
    };

    // Play function to start playback 
    play() {

        this.enablePlay = true;

        // 				this.cursor = index;
        console.log("play");

        this.stop = setInterval(() => {
            this.drawMarker(this.map, this.cursor);
            this.drawLine(this.map, this.cursor);

            if (this.cursor == this.points.length - 1) {
                console.log("STOPPPPPPPPPPPPPP")
                this.stop();
            }

            console.log(this.cursor);
            this.cursor++;
            // 					}
        }, 2000);
    }

    // Pause function and get index of last position 
    pause() {
        this.enablePlay = false;
        console.log("pause");
        clearTimeout(this.stop);
        this.stop = undefined;
    }

    // Stop function abd reset counter 
    stopPlayback() {
        this.enablePlay = false;
        console.log("stop");
        clearTimeout(this.stop);
        this.stop = undefined;
        this.cursor = 0;
    }

    // Clear Map 
    clearMap() {
        // Remove all markers
        for (var i = 0; i < this.markers.length; i++) {
            this.map.removeLayer(this.markers[i]);
        }

        // Remove all lines
        for (var i = 0; i < this.polylines.length; i++) {
            this.map.removeLayer(this.polylines[i]);
        }
    };






}
