import { Component, OnInit } from '@angular/core';
import * as L from 'leaflet';

@Component({
  moduleId: module.id,
  selector: 'maps-cmp',
  templateUrl: 'maps.component.html',
  styleUrls: ['./map.component.scss']
})

export class MapsComponent implements OnInit {
  name = 'Angular';
  map: any;

  ngOnInit() {
    this.map = L.map('map-l', {
      center: [24.64015, 46.70357],
      zoom: 13
    });

    const tiles = L.tileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
      maxZoom: 19,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    tiles.addTo(this.map);

    var greendotlayer, greendots, greydotlayer, greydots, bluedotlayer, bluedots,
    reddotlayer, reddots, greendotcount = 500, reddotcount = 150 , greydotcount = 1000, bluedotcount = 300;
    var centerlat = 24.64015, centerlon = 46.70357;
    make_greendots(), make_reddots(),  make_greydots(),  make_bluedots();
    var greendotStyleDefault = {
      radius: 5,
      fillColor: 'green',
      color: "#000",
      weight: 0,
      opacity: 1,
      fillOpacity: .9
    }, greendotStyleHighlight = {
      radius: 6,
      fillColor: "green",
      color: "#FFF",
      weight: 1,
      opacity: 1,
      fillOpacity: .9
    }, reddotStyleDefault = {
      radius: 5,
      fillColor: "red",
      color: "#FFF",
      weight: 0,
      opacity: 1,
      fillOpacity: .9
    }, reddotStyleHighlight = {
      radius: 6,
      fillColor: "#da3e7b",
      color: "#FFF",
      weight: 1,
      opacity: 1,
      fillOpacity: .9
    }, greydotStyleDefault = {
      radius: 5,
      fillColor: 'grey',
      color: "#000",
      weight: 0,
      opacity: 1,
      fillOpacity: .9
    }, greydotStyleHighlight = {
      radius: 6,
      fillColor: "grey",
      color: "#FFF",
      weight: 1,
      opacity: 1,
      fillOpacity: .9
    }, bluedotStyleDefault = {
      radius: 5,
      fillColor: 'blue',
      color: "#000",
      weight: 0,
      opacity: 1,
      fillOpacity: .9
    }, bluedotStyleHighlight = {
      radius: 6,
      fillColor: "blue",
      color: "#FFF",
      weight: 1,
      opacity: 1,
      fillOpacity: .9
    };

    greendotlayer = L.geoJson(greendots, {
      pointToLayer: function (feature, latlng) {
        return L.circleMarker(latlng, greendotStyleDefault);
      },
      onEachFeature: onEachgreendot
    }).addTo(this.map);
    
    reddotlayer = L.geoJson(reddots, {
      pointToLayer: function (feature, latlng) {
        return L.circleMarker(latlng, reddotStyleDefault);
      },
      onEachFeature: onEachreddot
    }).addTo(this.map);

    greydotlayer = L.geoJson(greydots, {
      pointToLayer: function (feature, latlng) {
        return L.circleMarker(latlng, greydotStyleDefault);
      },
      onEachFeature: onEachgreendot
    }).addTo(this.map), 
    

    bluedotlayer = L.geoJson(bluedots, {
      pointToLayer: function (feature, latlng) {
        return L.circleMarker(latlng, bluedotStyleDefault);
      },
      onEachFeature: onEachgreendot
    }).addTo(this.map);

    function normish(mean, range) {
      var num_out = (Math.random() + Math.random() + Math.random() + Math.random() - 2) / 2 * range + mean;
      return num_out;
    }

    function make_greendots() {
      greendots = {
        type: "FeatureCollection",
        features: []
      };
      for (var i = 0; i < greendotcount; ++i) {
        const x = normish(0, 1), y = normish(0, 1);
        var g = {
          type: "Point",
          coordinates: [.1 * x + centerlon, .1 * y + centerlat]
        }, p = {
          id: i,
          popup: "Mohamed_" + i,
          // tslint:disable-next-line: radix
          year: parseInt('' + (100 * Math.sqrt(x * x + y * y) * (1 - Math.random() / 2) + 1900))
        };
        greendots.features.push({
          geometry: g,
          type: "Feature",
          properties: p
        });
      }
    }

    function make_bluedots() {
      bluedots = {
        type: "FeatureCollection",
        features: []
      };
      for (var i = 0; i < bluedotcount; ++i) {
        const x = normish(0, 1), y = normish(0, 1);
        var g = {
          type: "Point",
          coordinates: [.1 * x + centerlon, .1 * y + centerlat]
        }, p = {
          id: i,
          popup: "Mohamed_" + i,
          // tslint:disable-next-line: radix
          year: parseInt('' + (100 * Math.sqrt(x * x + y * y) * (1 - Math.random() / 2) + 1900))
        };
        bluedots.features.push({
          geometry: g,
          type: "Feature",
          properties: p
        });
      }
    }

    function make_greydots() {
      greydots = {
        type: "FeatureCollection",
        features: []
      };
      for (var i = 0; i < greydotcount; ++i) {
        const x = normish(0, 1), y = normish(0, 1);
        var g = {
          type: "Point",
          coordinates: [.1 * x + centerlon, .1 * y + centerlat]
        }, p = {
          id: i,
          popup: "Mohamed_" + i,
          // tslint:disable-next-line: radix
          year: parseInt('' + (100 * Math.sqrt(x * x + y * y) * (1 - Math.random() / 2) + 1900))
        };
        greydots.features.push({
          geometry: g,
          type: "Feature",
          properties: p
        });
      }
    }

    function make_reddots() {
      reddots = {
        type: "FeatureCollection",
        features: []
      };
      for (var i = 0; i < reddotcount; ++i) {
        const x = normish(0, 1), y = normish(0, 1);
        var g = {
          type: "Point",
          coordinates: [.05 * x + centerlon + .05, .05 * y + centerlat]
        }, p = {
          id: i,
          popup: "Khaled_" + i,
          // tslint:disable-next-line: radix
          year: parseInt('' + (100 * Math.sqrt(x * x + y * y) * (1 - Math.random() / 2) + 1900))
        };
        reddots.features.push({
          geometry: g,
          type: "Feature",
          properties: p
        });
      }
    }

    function highlightgreendot(e) {
      var layer = e.target;
      layer.setStyle(greendotStyleHighlight), L.Browser.ie || L.Browser.opera || layer.bringToFront();
    }
    function resetgreendotHighlight(e) {
      var layer = e.target;
      layer.setStyle(greendotStyleDefault);
    }
    function onEachgreendot(feature, layer) {
      layer.on({
        mouseover: highlightgreendot,
        mouseout: resetgreendotHighlight
      }), layer.bindPopup('<table style="width:150px"><tbody><tr><td><div><b>name:</b></div></td><td><div>' + feature.properties.popup + "</div></td></tr><tr class><td><div><b>year:</b></div></td><td><div>" + feature.properties.year + "</div></td></tr></tbody></table>");
    }
    function highlightreddot(e) {
      var layer = e.target;
      layer.setStyle(reddotStyleHighlight), L.Browser.ie || L.Browser.opera || layer.bringToFront();
    }
    function resetreddotHighlight(e) {
      var layer = e.target;
      layer.setStyle(reddotStyleDefault);
    }
    function onEachreddot(feature, layer) {
      layer.on({
        mouseover: highlightreddot,
        mouseout: resetreddotHighlight
      }), layer.bindPopup('<table style="width:150px"><tbody><tr><td><div><b>name:</b></div></td><td><div>' + feature.properties.popup + "</div></td></tr><tr class><td><div><b>year:</b></div></td><td><div>" + feature.properties.year + "</div></td></tr></tbody></table>");
    }

  }




}
